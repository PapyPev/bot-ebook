Bot-Ebook
=========

Ce script permet de parser un fichier HTML contenu dans le répertoire "./html" d'en extraire les propriétés <code>href</code> des balises ```<a>``` puis de télécharger les fichiers finissant par ".epub".
