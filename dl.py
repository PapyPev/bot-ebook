#!/usr/bin/python
#-*- coding: utf-8 -*-

# Here : http://sonia.ombres.org/00-livres/000000-livres/Integrale%20ebooks%20Francais%20(2500%20epub)%20team%20AlexandriZ/

# =============================================================================
# LIBRARIES
# =============================================================================

import os
import urllib
import logging
from HTMLParser import HTMLParser

# =============================================================================
# GLOBALES
# =============================================================================

# Init log file
log_file = "{}{}{}".format(os.getcwd(), os.sep, "dl.log")

# Init html directory
dir_html = "{}{}{}".format(os.getcwd(), os.sep, "html")

# Init download directory
root_down = "{}{}{}".format(os.getcwd(), os.sep, "download")

# Current download directory
dir_down = ""


# =============================================================================
# CLASSES
# =============================================================================


class MyHTMLParser(HTMLParser):

    # -------------------------------------------------------------------------
    def __init__(self):
        HTMLParser.__init__(self)
        self.inLink = False
        self.dataArray = []
        self.lasttag = None
        self.lastname = None
        self.lastvalue = None
        self.link = None
        self.fileformat = None
        self.counter = 0

    # -------------------------------------------------------------------------
    def handle_starttag(self, tag, attrs):
        self.inLink = False
        if tag == "a":
            for attr in attrs:
                if attr[0] == "href" and attr[1].endswith(".epub"):
                    self.inLink = True
                    self.counter += 1
                    self.lasttag = tag
                    self.link = attr[1]
                if attr[1].endswith(".epub"):
                    self.fileformat = ".epub"
                else:
                    self.fileformat = ".nfo"

    # -------------------------------------------------------------------------
    def handle_endtag(self, tag):
        if tag == "a":
            self.inlink = False

    # -------------------------------------------------------------------------
    def handle_data(self, data):
        if self.lasttag == 'a' and self.inLink and data.strip():
            console("  [{}] -> {}".format(self.counter, data))
            url_file = urllib.URLopener()
            url_file.retrieve(self.link, "{}{}{}".format(
                dir_down, os.sep, data))

    # def handle_comment(self, data):
    #     console "Comment  :", data

    # def handle_entityref(self, name):
    #     c = unichr(name2codepoint[name])
    #     console "Named ent:", c

    # def handle_charref(self, name):
    #     if name.startswith('x'):
    #         c = unichr(int(name[1:], 16))
    #     else:
    #         c = unichr(int(name))
    #     console "Num ent  :", c

    # def handle_decl(self, data):
    #     console "Decl     :", data


# =============================================================================
# FUNCTIONS
# =============================================================================

def console(text):
    print(text)
    logging.info(text)

# -----------------------------------------------------------------------------


def main():

    # Init edited globals
    global dir_down

    # Loop html directory
    for subdir, dirs, files in os.walk(dir_html):

        # Loop all files in directory
        for file in files:

            # Check only html files
            if file.endswith(".html"):
                data = ""
                file_path = "{}{}{}".format(dir_html, os.sep, file)
                type_name = file.split(".")[0]
                dir_down = "{}{}{}".format(root_down, os.sep, type_name)
                console(type_name)

                # Create type directory if not exists
                if not os.path.exists(dir_down):
                    os.makedirs(dir_down)

                # Read html file as data
                with open(file_path, 'r') as html_file:
                    data = html_file.read().replace('\n', '')

                # Instantiate the parser and fed it some HTML
                parser = MyHTMLParser()
                parser.feed(data)


# =============================================================================
# MAIN PROGRAM
# =============================================================================


if __name__ == "__main__":

    # Init log
    if os.path.exists(log_file):
        os.remove(log_file)
    logging.basicConfig(
        filename=log_file,
        format="%(asctime)s | %(message)s",
        level=logging.DEBUG)

    # Run
    main()
